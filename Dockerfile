ARG ODOO_IMAGE_TYPE="odoo-ci"
ARG ODOO_VERSION="17.0"
FROM registry.crnd.pro/crnd-opensource/docker/${ODOO_IMAGE_TYPE}:${ODOO_VERSION}
MAINTAINER CRnD
