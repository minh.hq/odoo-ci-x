# Odoo CI X

This image is intended to be used in CI flows.
It contains tags, that are build on top of [Odoo Simple](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-simple) and [Odoo CI](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-ci) images.

Images marked as `-simple` are based on [Odoo Simple](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-simple) image.
Contrary images marked by `-ci` are based on [Odoo CI](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-ci)

Images marked as `-production` are based on [Odoo Production](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-production) image.

Look at available tags on [CR&D's GitLab](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-ci-x/container_registry)

## Available tags

For available tags look [here](https://gitlab.crnd.pro/crnd-opensource/docker/odoo-simple/container_registry)

| Tag            | Description                    | Note                                |
|----------------|--------------------------------|-------------------------------------|
|10.0            | CI image for Odoo 10.0         | Postgresql server included in image |
|11.0            | CI image for Odoo 11.0         | Postgresql server included in image |
|12.0            | CI image for Odoo 12.0         | Postgresql server included in image |
|13.0            | CI image for Odoo 13.0         | Postgresql server included in image |
|14.0            | CI image for Odoo 14.0         | Postgresql server included in image |
|15.0            | CI image for Odoo 15.0         | Postgresql server included in image |
|10.0-ci         | CI image for Odoo 10.0         | Postgresql server included in image |
|11.0-ci         | CI image for Odoo 11.0         | Postgresql server included in image |
|12.0-ci         | CI image for Odoo 12.0         | Postgresql server included in image |
|13.0-ci         | CI image for Odoo 13.0         | Postgresql server included in image |
|14.0-ci         | CI image for Odoo 14.0         | Postgresql server included in image |
|15.0-ci         | CI image for Odoo 15.0         | Postgresql server included in image |
|10.0-simple     | Simple image for Odoo 10.0     |                                     |
|11.0-simple     | Simple image for Odoo 11.0     |                                     |
|12.0-simple     | Simple image for Odoo 12.0     |                                     |
|13.0-simple     | Simple image for Odoo 13.0     |                                     |
|14.0-simple     | Simple image for Odoo 14.0     |                                     |
|15.0-simple     | Simple image for Odoo 15.0     |                                     |
|10.0-production | Production image for Odoo 10.0 |                                     |
|11.0-production | Production image for Odoo 11.0 |                                     |
|12.0-production | Production image for Odoo 12.0 |                                     |
|13.0-production | Production image for Odoo 13.0 |                                     |
|14.0-production | Production image for Odoo 14.0 |                                     |
|15.0-production | Production image for Odoo 15.0 |                                     |

## Configuration
Each image could be configured using following environment variables:

| Env variable             | Default    | Description |
|:-------------------------|:-----------|:------------|
| ODOO_DB_HOST             | localhost  | Database host to connect to |
| ODOO_DB_PORT             | 5432       | Database port to connect to |
| ODOO_DB_USER             | odoo       | Database user login to connect with |
| ODOO_DB_PASSWORD         | odoo       | Database user password to conenct with |
| ODOO_DB_TEMPLATE         | template1  | Odoo database template to create new database |
| ODOO_DB_MAXCONN          | 64         | Max database connections can be used by Odoo |
| ODOO_DB_NAME             | False      | Database name for mono-db setup
| ODOO_ADMIN_PASS          |            | Odoo instance admin password |
| ODOO_DEBUG_MODE          | True       | Enable od disable debug mode |
| ODOO_DB_FILTER           | '.\*'      | Database filter mask |
| ODOO_RUN_PARAMS          |            | Params to run odoo with. For example ``--workers=4``, etc |
| ODOO_SMTP_USER           | False      | SMTP user (set in odoo conf, but seems not to be used) |
| ODOO_SMTP_PASSWORD       | False      | SMTP password (set in odoo conf, but seems not to be used) |
| ODOO_SMTP_SERVER         | localhost  | SMTP server (set in odoo conf, but seems not to be used) |
| ODOO_SMTP_PORT           | 25         | SMTP port (set in odoo conf, but seems not to be used) |
| ODOO_SMTP_SSL            | False      | SMTP Enable or disable SSL (set in odoo conf, but seems not to be used) |
| ODOO_EMAIL_FROM          | False      | Default Email from address (set in odoo conf, but seems not to be used) |
| ODOO_LIST_DB             | True       | Allow or deny clients to get list of databases (for example via RPC) |
| ODOO_MAX_CRON_THREADS    | 2          | Max parallel cron workers allowed |
| ODOO_PROXY_MODE          | False      | Enable correct behavior when behind a reverse proxy |
| ODOO_WORKERS             | 0          | Run odoo in prefork mode |
| ODOO_LIMIT_MEMORY_HARD   |            | Optional |
| ODOO_LIMIT_MEMORY_SOFT   |            | Optional |
| ODOO_LIMIT_REQUEST       |            | Optional |
| ODOO_LIMIT_TIME_CPU      |            | Optional |
| ODOO_LIMIT_TIME_REAL     |            | Optional |
| ODOO_SERVER_WIDE_MODULES | base,web   | Optional. String with comma-separated list of server-wide addons |
| ODOO_EXTRA_CONFIG        |            | Optional. String with extra data to be placed in config |


## Yodoo Cockpit

[![Yodoo Cockpit](https://crnd.pro/web/image/18846/banner_2_4_gif_animation_cut.gif)](https://crnd.pro/yodoo-cockpit)

Take a look at [Yodoo Cockpit](https://crnd.pro/yodoo-cockpit) project, and discover the easiest way to manage your odoo installation.
Just short notes about [Yodoo Cockpit](https://crnd.pro/yodoo-cockpit):
- start new production-ready odoo instance in 1-2 minutes.
- add custom addons to your odoo instances in a few clicks.
- out-of-the-box email configuration: just press button and add some records to your DNS, and get a working email
- make your odoo instance available to external world (internet) in 30 seconds (just add single record in your DNS)
- easy way to sell your odoo-based product in SaaS way.
- Yodoo Cockpit could be launched as Infrastructure as a Service (IaaS), thus you even do not need to worry about servers.

If you have any questions, then contact us at [info@crnd.pro](mailto:info@crnd.pro), so we could schedule online-demonstration.

## Level up your service quality

Level up your service with our [Helpdesk](https://crnd.pro/solutions/helpdesk) / [Service Desk](https://crnd.pro/solutions/service-desk) / [ITSM](https://crnd.pro/itsm) solution.

Just test it at [yodoo.systems](https://yodoo.systems/saas/templates): choose template you like, and start working.

Test all available features of [Bureaucrat ITSM](https://crnd.pro/itsm) with [this template](https://yodoo.systems/saas/template/bureaucrat-itsm-demo-data-95).## Level up your service quality

Level up your service with our [Helpdesk](https://crnd.pro/solutions/helpdesk) / [Service Desk](https://crnd.pro/solutions/service-desk) / [ITSM](https://crnd.pro/itsm) solution.

Just test it at [yodoo.systems](https://yodoo.systems/saas/templates): choose template you like, and start working.

Test all available features of [Bureaucrat ITSM](https://crnd.pro/itsm) with [this template](https://yodoo.systems/saas/template/bureaucrat-itsm-demo-data-95).

## Bug tracker

Bugs are tracked on [https://crnd.pro/requests](https://crnd.pro/requests>).
In case of trouble, please report there.

## Maintainer

![Center of Research & Development](https://crnd.pro/web/image/3699/300x140/crnd.png)

Our web site is: [crnd.pro](https://crnd.pro/)

This module is maintained by the [Center of Research & Development](https://crnd.pro) company.

We can provide you further Odoo Support, Odoo implementation, Odoo customization, Odoo 3rd Party development and integration software, consulting services (more info available on [our site](https://crnd.pro/our-services)).Our main goal is to provide the best quality product for you. 

For any questions [contact us](mailto:info@crnd.pro>).
